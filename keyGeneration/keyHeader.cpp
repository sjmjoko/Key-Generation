#ifndef KEYHEADER_H
#define KEYHEADER_H
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include<bitset>
#include<vector>
#include<array>
#include<algorithm>

using namespace std;
vector< vector<int> > encryptionKey, tempKey;    //2D  encryption key
vector<char> key;
vector<int> asciiKey, PC_1_key, x;
int kTemp[64];
//PC-1 standard table
int pc_1_Table[] = { 57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44,
36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4 };
//PC-2 standard table
int pc_2_Table[] = { 14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 32, 37, 47, 55, 30,
40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32 };
int ascii, q = 0;
string  originalKey;
template < class T >
void print(vector<T> v);
template< class X >
void print2D(vector< vector<X>> x);
vector<int> toASCII(vector<int> &v, vector<char> &key);
int toBINARY(vector<int> &ascii, int temp[]);
template<class Y>
vector<vector<int> > Resize(vector<vector<Y> > &v, int rows, int columns);
vector<int> pc1(int kt[], vector<int> &pk, int a[]);
vector<vector<int> > rotationSchedule(vector<vector<int> > &tk, vector<int> &pc, vector<int> x);
vector<vector<int> > pc2(vector<vector<int> > &v, vector<vector<int> > &k, int a[]);

#endif